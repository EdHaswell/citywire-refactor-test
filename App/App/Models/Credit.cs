﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Models
{
    public class Credit
    {
        public bool HasCreditLimit { get; set; }

        public int? CreditLimit { get; set; } // Set credit limit to nullable
    }
}
