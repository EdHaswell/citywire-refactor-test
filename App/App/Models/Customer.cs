using App.Models;
using System;

namespace App
{
    public class Customer : CustomerDetails
    {
        public int Id { get; set; }

        public Credit Credit { get; set; }

        public Company Company { get; set; }
    }
}