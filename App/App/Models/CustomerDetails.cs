﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App
{
    public class CustomerDetails
    {
        public string Firstname { get; set; }

        public string Surname { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string EmailAddress { get; set; }

        public void Validate() 
        {
            string errorList = string.Empty;

            if (string.IsNullOrEmpty(Firstname) || string.IsNullOrEmpty(Surname))
            {
                errorList += "Firstname and surname must be provided. ";
            }

            if (!EmailAddress.Contains("@") || !EmailAddress.Contains(".")) // Changed from && to || as either condition breaks email format standards
            {
                errorList += "Invalid email provided. ";
            }

            var now = DateTime.Today;
            int age = now.Year - DateOfBirth.Year;
            if (DateOfBirth.Date > now.AddYears(-age)) 
                age--;

            if (age < 21)
            {
                errorList += "Customer does not meet age requirements.";
            }

            if (errorList != string.Empty) 
            {
                throw new Exception(errorList);
            }
        }
    }
}
