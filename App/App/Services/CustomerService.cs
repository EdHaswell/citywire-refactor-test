﻿using App.Models;
using App.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static App.CompanyRepository;

namespace App
{
    /// <summary>
    /// Customer Service Note: I dont usually comment this much, I did so in this case so you can better understand the decisions I took
    /// </summary>
    public class CustomerService
    {
        private readonly ICustomerCreditService customerCreditService;
        private readonly ICompanyRepository companyRepository;
        private readonly ICustomerDataAccessWrapper customerDataAccessWrapper;

        // Added constructor to inject consumed classes
        public CustomerService (
            ICustomerCreditService customerCreditService,  
            ICompanyRepository companyRepository, 
            ICustomerDataAccessWrapper customerDataAccessWrapper)
        {
            this.customerCreditService = customerCreditService;
            this.companyRepository = companyRepository;
            this.customerDataAccessWrapper = customerDataAccessWrapper;
        }
        public Customer AddCustomer(CustomerDetails customerDetails, int companyId) // Collapsed parameters into Customer details object which the Customer object inherits
        {
            // Moved validation code to customur details model
            customerDetails.Validate();

            var company = this.companyRepository.GetById(companyId);
            var credit = this.SetCreditValues(customerDetails, company);

            // While not critical in such a small use case I would swap to use an ORM like Automapper so the mapping logic can be abstracted away
            var customer = new Customer
            {
                Company = company,
                DateOfBirth = customerDetails.DateOfBirth,
                EmailAddress = customerDetails.EmailAddress,
                Firstname = customerDetails.Firstname,
                Surname = customerDetails.Surname,
                Credit = credit
            };

            // Wrapped in non static class and injected into class rather than direct call
            // Given more time I would have modified to return the created object with the new ids and then returned that
            customerDataAccessWrapper.AddCustomer(customer);

            // Return the customer object rather than true or false (preferably with returned entity from AddCustomer() so methods consuming it can identify the new entity created)
            return customer;
        }

        // Set the credit information
        private Credit SetCreditValues(CustomerDetails customerDetails, Company company) 
        {
            int? creditLimit = this.customerCreditService.GetCreditLimit(customerDetails.Firstname, customerDetails.Surname, customerDetails.DateOfBirth);

            // Set hasCreditLimit to true by default if user is a Gold User change it to false
            var hasCreditLimit = true;

            // Switched to switch/case over multiple if statements
            switch (company.Classification)
            {
                case Classification.Gold:
                    hasCreditLimit = false;
                    creditLimit = null; // Remove limit if gold seems weird to have a credit limit of 0
                    break;
                case Classification.Silver:
                    creditLimit = creditLimit * 2;
                    break;
                case Classification.Bronze:
                    // No need for Bronze case as it has nothing different from the default values, can serve as placeholder.
                    break;
            }

            if (hasCreditLimit && creditLimit < 500)
            {
                throw new Exception("Client does not meet credit requirements"); // Changed from return false to throw exceptions (in order to better notify the user)
            }

            return new Credit
            {
                HasCreditLimit = hasCreditLimit,
                CreditLimit = creditLimit
            };
        }
    }
}
