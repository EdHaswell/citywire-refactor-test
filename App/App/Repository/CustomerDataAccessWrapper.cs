﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository
{
    public interface ICustomerDataAccessWrapper
    {
        void AddCustomer(Customer customer);
    }

    public class CustomerDataAccessWrapper : ICustomerDataAccessWrapper
    {
        public void AddCustomer(Customer customer) 
        {
            // Given more time and without the criteria of the spec to not change Class from static, I would have thrown this entire class away in favour of moving to entity framework
            CustomerDataAccess.AddCustomer(customer);
        }
    }
}
