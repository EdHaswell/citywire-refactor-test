﻿using App.Repository;
using Moq;
using NUnit.Framework;
using Should;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static App.CompanyRepository;

namespace App.ServiceTests
{
    // For the tests I went with NUnit
    [TestFixture]
    public class CustomerServiceTests
    {
        [Test]
        public void CustomerService_AddCustomer_AddOneCustomerWithBronze_Ok() 
        {
            // Arrange
            var newCustomer = new CustomerDetails
            {
                DateOfBirth = DateTime.Today.AddYears(-40),
                Firstname = "Ben",
                Surname = "Wade",
                EmailAddress = "Ben.Wade@Yuma.com"
            };

            Mock<ICustomerCreditService> mockCustomerCreditService = new Mock<ICustomerCreditService>();
            var customerCreditService = mockCustomerCreditService.Object;

            mockCustomerCreditService.Setup(x => x.GetCreditLimit(newCustomer.Firstname, newCustomer.Surname, newCustomer.DateOfBirth)).Returns(1000);

            Mock<ICompanyRepository> mockCompanyRepository = new Mock<ICompanyRepository>();
            var companyRepository = mockCompanyRepository.Object;

            mockCompanyRepository.Setup(x => x.GetById(1)).Returns(new Company { Id = 1, Classification = Classification.Bronze, Name = "Company1" });

            Mock<ICustomerDataAccessWrapper> mockCustomerDataAccess = new Mock<ICustomerDataAccessWrapper>();
            var customerDataAccessWrapper = mockCustomerDataAccess.Object;

            var service = new CustomerService(customerCreditService, companyRepository, customerDataAccessWrapper);

            // Act
            var result = service.AddCustomer(newCustomer, 1);

            // Assert
            mockCustomerCreditService.Verify(x => x.GetCreditLimit(newCustomer.Firstname, newCustomer.Surname, newCustomer.DateOfBirth), Times.Once);
            mockCompanyRepository.Verify(x => x.GetById(1), Times.Once);
            mockCustomerDataAccess.Verify(x => x.AddCustomer(result), Times.Once);

            result.Credit.HasCreditLimit.ShouldEqual(true);
            result.Credit.CreditLimit.ShouldEqual(1000);
        }

        [Test]
        public void CustomerService_AddCustomer_AddOneCustomerWithSilverClassification_Ok()
        {
            // Arrange
            var newCustomer = new CustomerDetails
            {
                DateOfBirth = DateTime.Today.AddYears(-40),
                Firstname = "Ben",
                Surname = "Wade",
                EmailAddress = "Ben.Wade@Yuma.com"
            };

            Mock<ICustomerCreditService> mockCustomerCreditService = new Mock<ICustomerCreditService>();
            var customerCreditService = mockCustomerCreditService.Object;

            mockCustomerCreditService.Setup(x => x.GetCreditLimit(newCustomer.Firstname, newCustomer.Surname, newCustomer.DateOfBirth)).Returns(1000);

            Mock<ICompanyRepository> mockCompanyRepository = new Mock<ICompanyRepository>();
            var companyRepository = mockCompanyRepository.Object;

            mockCompanyRepository.Setup(x => x.GetById(1)).Returns(new Company { Id = 1, Classification = Classification.Silver, Name = "Company1" });

            Mock<ICustomerDataAccessWrapper> mockCustomerDataAccess = new Mock<ICustomerDataAccessWrapper>();
            var customerDataAccessWrapper = mockCustomerDataAccess.Object;

            var service = new CustomerService(customerCreditService, companyRepository, customerDataAccessWrapper);

            // Act
            var result = service.AddCustomer(newCustomer, 1);

            // Assert
            mockCustomerCreditService.Verify(x => x.GetCreditLimit(newCustomer.Firstname, newCustomer.Surname, newCustomer.DateOfBirth), Times.Once);
            mockCompanyRepository.Verify(x => x.GetById(1), Times.Once);
            mockCustomerDataAccess.Verify(x => x.AddCustomer(result), Times.Once);

            result.Credit.HasCreditLimit.ShouldEqual(true);
            result.Credit.CreditLimit.ShouldEqual(2000);
        }

        [Test]
        public void CustomerService_AddCustomer_AddOneCustomerWithGoldClassification_Ok()
        {
            // Arrange
            var newCustomer = new CustomerDetails
            {
                DateOfBirth = DateTime.Today.AddYears(-40),
                Firstname = "Ben",
                Surname = "Wade",
                EmailAddress = "Ben.Wade@Yuma.com"
            };

            Mock<ICustomerCreditService> mockCustomerCreditService = new Mock<ICustomerCreditService>();
            var customerCreditService = mockCustomerCreditService.Object;

            mockCustomerCreditService.Setup(x => x.GetCreditLimit(newCustomer.Firstname, newCustomer.Surname, newCustomer.DateOfBirth)).Returns(1000);

            Mock<ICompanyRepository> mockCompanyRepository = new Mock<ICompanyRepository>();
            var companyRepository = mockCompanyRepository.Object;

            mockCompanyRepository.Setup(x => x.GetById(1)).Returns(new Company { Id = 1, Classification = Classification.Gold, Name = "Company1" });

            Mock<ICustomerDataAccessWrapper> mockCustomerDataAccess = new Mock<ICustomerDataAccessWrapper>();
            var customerDataAccessWrapper = mockCustomerDataAccess.Object;

            var service = new CustomerService(customerCreditService, companyRepository, customerDataAccessWrapper);

            // Act
            var result = service.AddCustomer(newCustomer, 1);

            // Assert
            mockCustomerCreditService.Verify(x => x.GetCreditLimit(newCustomer.Firstname, newCustomer.Surname, newCustomer.DateOfBirth), Times.Once);
            mockCompanyRepository.Verify(x => x.GetById(1), Times.Once);
            mockCustomerDataAccess.Verify(x => x.AddCustomer(result), Times.Once);

            result.Credit.HasCreditLimit.ShouldEqual(false);
            result.Credit.CreditLimit.ShouldBeNull();
        }

        [Test]
        public void CustomerService_AddCustomer_AddCustomerWithInvalidDoB_Exception()
        {
            // Arrange
            var newCustomer = new CustomerDetails
            {
                DateOfBirth = DateTime.Today.AddYears(-10),
                Firstname = "Ben",
                Surname = "Wade",
                EmailAddress = "Ben.Wade@Yuma.com"
            };

            Mock<ICustomerCreditService> mockCustomerCreditService = new Mock<ICustomerCreditService>();
            var customerCreditService = mockCustomerCreditService.Object;

            mockCustomerCreditService.Setup(x => x.GetCreditLimit(newCustomer.Firstname, newCustomer.Surname, newCustomer.DateOfBirth)).Returns(1000);

            Mock<ICompanyRepository> mockCompanyRepository = new Mock<ICompanyRepository>();
            var companyRepository = mockCompanyRepository.Object;

            mockCompanyRepository.Setup(x => x.GetById(1)).Returns(new Company { Id = 1, Classification = Classification.Bronze, Name = "Company1" });

            Mock<ICustomerDataAccessWrapper> mockCustomerDataAccess = new Mock<ICustomerDataAccessWrapper>();
            var customerDataAccessWrapper = mockCustomerDataAccess.Object;

            var service = new CustomerService(customerCreditService, companyRepository, customerDataAccessWrapper);

            // Act
            var exception = Assert.Throws<Exception>(() => service.AddCustomer(newCustomer, 1));
            exception.Message.ShouldEqual("Customer does not meet age requirements.");
        }

        [Test]
        public void CustomerService_AddCustomer_AddCustomerWithBadEmail_Exception()
        {
            // Arrange
            var newCustomer = new CustomerDetails
            {
                DateOfBirth = DateTime.Today.AddYears(-40),
                Firstname = "Ben",
                Surname = "Wade",
                EmailAddress = "Ben.WadeYuma.com"
            };

            Mock<ICustomerCreditService> mockCustomerCreditService = new Mock<ICustomerCreditService>();
            var customerCreditService = mockCustomerCreditService.Object;

            mockCustomerCreditService.Setup(x => x.GetCreditLimit(newCustomer.Firstname, newCustomer.Surname, newCustomer.DateOfBirth)).Returns(1000);

            Mock<ICompanyRepository> mockCompanyRepository = new Mock<ICompanyRepository>();
            var companyRepository = mockCompanyRepository.Object;

            mockCompanyRepository.Setup(x => x.GetById(1)).Returns(new Company { Id = 1, Classification = Classification.Bronze, Name = "Company1" });

            Mock<ICustomerDataAccessWrapper> mockCustomerDataAccess = new Mock<ICustomerDataAccessWrapper>();
            var customerDataAccessWrapper = mockCustomerDataAccess.Object;

            var service = new CustomerService(customerCreditService, companyRepository, customerDataAccessWrapper);

            // Act
            var exception = Assert.Throws<Exception>(() => service.AddCustomer(newCustomer, 1));
            exception.Message.ShouldEqual("Invalid email provided. ");
        }

        [Test]
        public void CustomerService_AddCustomer_AddCustomerWithNoName_Exception()
        {
            // Arrange
            var newCustomer = new CustomerDetails
            {
                DateOfBirth = DateTime.Today.AddYears(-40),
                Firstname = string.Empty,
                Surname = "Wade",
                EmailAddress = "Ben.Wade@Yuma.com"
            };

            Mock<ICustomerCreditService> mockCustomerCreditService = new Mock<ICustomerCreditService>();
            var customerCreditService = mockCustomerCreditService.Object;

            mockCustomerCreditService.Setup(x => x.GetCreditLimit(newCustomer.Firstname, newCustomer.Surname, newCustomer.DateOfBirth)).Returns(1000);

            Mock<ICompanyRepository> mockCompanyRepository = new Mock<ICompanyRepository>();
            var companyRepository = mockCompanyRepository.Object;

            mockCompanyRepository.Setup(x => x.GetById(1)).Returns(new Company { Id = 1, Classification = Classification.Bronze, Name = "Company1" });

            Mock<ICustomerDataAccessWrapper> mockCustomerDataAccess = new Mock<ICustomerDataAccessWrapper>();
            var customerDataAccessWrapper = mockCustomerDataAccess.Object;

            var service = new CustomerService(customerCreditService, companyRepository, customerDataAccessWrapper);

            // Act
            var exception = Assert.Throws<Exception>(() => service.AddCustomer(newCustomer, 1));
            exception.Message.ShouldEqual("Firstname and surname must be provided. ");
        }
    }
}
